# vugu-tutorial

[vugu](https://www.vugu.org/)を使って作るSPA

## 準備

* Go 1.12~

## 手順

1. 空っぽのディレクトリを作成する
2. 空っぽの`go.mod`を作る

    ```$ go mod init```

3. `root.vugu`を設置
4. サーバを立てるのに必要なパッケージを取り込む

    ```$ go get github.com/vugu/vugu/simplehttp```

5. `devserver.go`を設置
6. サーバ起動

    ```$ go run devserver.go```

7. アクセス [http://127.0.0.1:8844/](http://127.0.0.1:8844/)

    `main_wasm.go`と`root.go`が生成され、Testボタンが表示されれば成功

## 失敗例

* `Error from ParserGoPkg: no .vugu files found, please create one and try again`
    * componentファイルの拡張子が`.vugu`じゃない

