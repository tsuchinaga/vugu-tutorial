// +build ignore

package main

import (
	"github.com/vugu/vugu/simplehttp"
	"log"
	"net/http"
	"os"
)

func main() {
	wd, _ := os.Getwd()
	l := "127.0.0.1:8844"
	log.Printf("Starting HTTP Server at %q\n", l)
	h := simplehttp.New(wd, true)
	log.Fatal(http.ListenAndServe(l, h))
}
